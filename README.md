# Sprites for PlantUML

https://github.com/cloudogu/plantuml-cloudogu-sprites

## Usage

First include the file common.puml either via path or url. It also contains style definitions according to the Cloudogu CI

```
[...]
!include ../common.puml
!includeurl https://raw.githubusercontent.com/cloudogu/plantuml-cloudogu-sprites/master/common.puml
[...]
```
You can define a macro for the URL

```
[...]
!define CLOUDOGUURL https://raw.githubusercontent.com/cloudogu/plantuml-cloudogu-sprites/master
!includeurl CLOUDOGUURL/common.puml
[...]
```

Then include the sprites that you want to use 

```
[...]
!includeurl CLOUDOGUURL/dogus/scm.puml
!includeurl CLOUDOGUURL/dogus/smeagol.puml
[...]
```

To use the Sprites you can include their name directly with <<$name>>

```
[...]
node "Cloudogu Ecosystem" <<$cloudogu>> {
[...]
```

or add one of the defined macros. Prefix can be either 'DOGU' or 'TOOL':

```
[...]
<prefix>_<name>(alias)
<prefix>_<name>(alias,label)
<prefix>_<name>(alias,label,shape)
<prefix>_<name>(alias,label,shape,color)
[...]
```

## Complete Example
```
@startuml
!define CLOUDOGUURL https://raw.githubusercontent.com/cloudogu/plantuml-cloudogu-sprites/master
!includeurl CLOUDOGUURL/common.puml
!includeurl CLOUDOGUURL/dogus/jenkins.puml
!includeurl CLOUDOGUURL/dogus/cloudogu.puml
!includeurl CLOUDOGUURL/dogus/scm.puml
!includeurl CLOUDOGUURL/dogus/smeagol.puml
!includeurl CLOUDOGUURL/dogus/nexus.puml
!includeurl CLOUDOGUURL/tools/k8s.puml

node "Cloudogu Ecosystem" <<$cloudogu>> {
	DOGU_JENKINS(jenkins, Jenkins) #ffffff
	DOGU_SCM(scm, SCM-Manager) #ffffff
	DOGU_SMEAGOL(smeagol, Smeagol) #ffffff
	DOGU_NEXUS(nexus,Nexus) #ffffff
}

TOOL_K8S(k8s, Kubernetes) #ffffff

actor developer

developer -> smeagol : "Edit Slides"
smeagol -> scm : Push
scm -> jenkins : Trigger
jenkins -> nexus : Deploy
jenkins -> k8s : Deploy

@enduml
```
## Adding new sprites

Only a few things are needed to create new sprites:

1. a 48x48 pixel bitmap image
1. the [plantuml.jar](https://sourceforge.net/projects/plantuml/files/plantuml.jar/download)
1. a working Java installation

Images work best when they have a white background (that is: no alpha/transparency channel is visible) and show a good contrast.

### Steps

1. prepare the input image
1. encode the input image to a Plant UML sprit`
   - `java -jar /path/to/plantuml.jar -encodesprite 16 yourSprite.png > yourSprite.puml`
1. optional, but it just would be nice:
   1. harmonize sprite file to match existing ones
      - add @startuml / @enduml tags
      - add ENTITY stuff (see the other .puml files)
   1. Share here
